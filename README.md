# Argility's First Bowling Kata

Kon'nichiwa (Good day)

KIO‑TSUKE (pronounced Kit-skay - means: Attention)

I introduce everyone to new initiative. It is for training discipline.

In this dojo you will learn different kata's (meaning: detailed choreographed patterns of movements practiced either solo or in pairs). You will also learn why kata's are important.

I (Leroux Michelson) will be your Sensei

Typically a kata should take 30-45mins.

## Getting Started

You will need laptop with gradle, eclipse and gradle plugin (Buildship). Make sure its working in new workspace called "dojo".

### Prerequisites

- [Eclipse IDE](https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/photonr) - Development IDE
- [Gradle Buildship Plugin](http://projects.eclipse.org/projects/tools.buildship) - Dependency Management

## Bowling Kata

The game consists of 10 frames. In each frame the player has two rolls to knock down 10 pins. The score for the frame is the total number of pins knocked down, plus bonuses for strikes and spares.

A spare is when the player knocks down all 10 pins in two rolls. The bonus for that frame is the number of pins knocked down by the next roll.

A strike is when the player knocks down all 10 pins on his first roll. The frame is then completed with a single roll. The bonus for that frame is the value of the next two rolls.

In the tenth frame a player who rolls a spare or strike is allowed to roll the extra balls to complete the frame. However no more than three balls can be rolled in tenth frame.

### Requirements

Write a class Game that has two methods

- void roll(int) is called each time the player rolls a ball. The argument is the number of pins knocked down.
- int score() returns the total score for that game.

## Quick Recap of our first session

### Test method naming convention

```
@Test
public void methodName_scenario_expectedOutcome() throws Exception {
}
```

### 3 A's of constructing a test

- **Arrange** all necessary preconditions and inputs.
- **Act** on the object or method under test.
- **Assert** that the expected results have occurred.

### The 3 rules of TDD

- You are not allowed to write any production code unless it is to make a failing unit test pass.
- You are not allowed to write any more of a unit test than is sufficient to fail; and compilation failures are failures.
- You are not allowed to write any more production code than is sufficient to pass the one failing unit test.

## Links

- [Bowling Game Kata](Bowling Game Kata - ButUncleBob) - Inspired by Uncle Bob
- **Uncle Bob Blog Site** - [butUncleBob](butunclebob.com)
