package com.argility.dojo.bowling;

public class Game {
	int[] rolls = new int[21];
	int idx = 0;

	public void roll(int pins) {
		rolls[idx++] = pins;
	}

	public int score() {
		int score = 0;
		int rollIndex = 0;

		for (int frame = 0; frame < 10; frame++) {
			if (isSpare(rollIndex)) {
				score += 10 + rolls[rollIndex + 2];
				rollIndex += 2;
			} else {
				score += frameScore(rollIndex);
				rollIndex += 2;
			}
		}

		return score;
	}

	private int frameScore(int rollIndex) {
		return rolls[rollIndex] + rolls[rollIndex + 1];
	}

	private boolean isSpare(int rollIndex) {
		return frameScore(rollIndex) == 10;
	}

}
