package com.argility.dojo.test.bowling;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.argility.dojo.bowling.Game;

public class gameTest {

	Game game;

	@Before
	public void setUp() {
		game = new Game();
	}

	// method_scenario_expected
	@Test
	public void score_gutterGame_shouldBeZero() throws Exception {

		rollMany(0, 20);

		// Assert - that the expected results have occurred.

		assertEquals(0, game.score());

	}

	private void rollMany(int pins, int numRolls) {
		for (int i = 0; i < numRolls; i++) {
			game.roll(pins);
		}
	}

	@Test
	public void score_rollAllOnes_shouldBeTwenty() throws Exception {

		rollMany(1, 20);

		assertEquals(20, game.score());

	}

	@Test
	public void score_rollSpareAndFour_shouldBeeighteen() throws Exception {

		rollSpare();

		game.roll(4);

		rollMany(0, 17);

		assertEquals(18, game.score());

	}

	private void rollSpare() {
		game.roll(5); // Spare
		game.roll(5);
	}

}
